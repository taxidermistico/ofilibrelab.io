---
layout: archive
permalink: /presentaciones/index
title: "Presentaciones de la OfiLibre"
image:
  feature: logo_horizontal_blanco.jpg
---

* [La OfiLibre](Ofilibre-presentacion.pdf):
  Presentación de la OfiLibre.
  [Versión ODP](Ofilibre-presentacion.odp)
  
* [Datos Abiertos](Datos_Abiertos.pdf)
  Presentación "Café y datos abiertos".
  [Versión ODP](Datos_Abiertos.odp)
  
* [Definición de Obra Abierta](Definiciones.pdf)
  Micropresentación "¿Qué es una obra abierta?".
  [Versión ODP](Definiciones.odp)
  