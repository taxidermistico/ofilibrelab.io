---
layout: archive
permalink: /guias/
title: "Guías de la OfiLibre"
image:
  feature: logo_horizontal_blanco.jpg
---

* [Guía sobre GIMP](gimp.html)
* [Guía sobre uso de issues en GitLab](guia_seguir_issue.md)
* [Listado de oficinas similares en otras Universidades](otras_oficinas.md)