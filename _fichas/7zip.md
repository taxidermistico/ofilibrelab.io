title: 7zip
logo: ../images/fichas/7zip/logo.png
website: https://www.7-zip.org/
website_es: 
licenses:
  - name: GPLv2
    url: https://es.wikipedia.org/wiki/GNU_General_Public_License
  - name: BSD
    url: https://es.wikipedia.org/wiki/Licencia_BSD
myapps: true
source: https://sourceforge.net/projects/sevenzip/files/
installs:
  - name: Cómo instalar 7zip
    url: http://www.gofree.com/Espanol/Descarga/7Zip/Tutoriales/instalararchivosZIP.php
    kind: web
tutorials:
  - name: Sobre comprimir y descomprimir
    url: https://www.youtube.com/watch?v=YV22NhyyUPc
    kind: video
  - name: Para comprimir mucho un archivo
    url: https://www.youtube.com/watch?v=rjomYa6K90s
    kind: video
others:

screenshots:
  - name: 7zip en funcionamiento (captura)
    file: captura.png
    attribution: "[Captura tomada de Sourceforge](https://sourceforge.net/p/sevenzip/screenshot/534500_4.png)

date: 2019-03-13
