title: OpenOffice
logo: ../images/fichas/OpenOffice/logo.png
website: https://www.openoffice.org/
website_es: https://www.openoffice.org/es/
licenses:
  - name: LGPLv3
    url: https://es.wikipedia.org/wiki/GNU_Lesser_General_Public_License
myapps: true
source: http://www.openoffice.org/download/source/
installs:
  - name: Cómo instalar OpenOffice
    url: https://www.openoffice.org/download/common/instructions.html
tutorials:
  - name: Tutorial de Writer
    url: https://www.youtube.com/playlist?list=PLLLaU95AMQPqAgeXCjQgXawAPABR19U2k
    kind: video
  - name: Tutorial de Calc
    url: https://www.youtube.com/playlist?list=PLGJdoR23n7rzhcAwusxmmL1tpaD-wy36t
    kind: video
  - name: Tutorial de impress
    url: https://www.youtube.com/playlist?list=PLULVVpt1gZKzr-BIhRLXTpb03YOFjdj-b
    kind: video  
  - name: Tutorial de Base
    url: https://www.youtube.com/playlist?list=PLLLaU95AMQPrMifyMRgiwhqKA64g7Kiea
    kind: video  
  - name: Tutorial de Draw
    url: https://www.youtube.com/playlist?list=PLLFD3chPm6dk9qSpQKfagKnUOSfugSv10
    kind: video  
  - name: Tutorial de Math
    url: https://www.youtube.com/watch?v=Lq3_XVxc6iM&list=PLu5LXaMUM_E1VgEoqXBsu758fB8FDymts
    kind: video
others:

screenshots:
  - name: Gimp en funcionamiento (captura)
    file: captura1.png
  - name: Gimp en funcionamiento (captura)
    file: captura2.png
  - name: Gimp en funcionamiento (captura)
    file: captura3.png
  - name: Gimp en funcionamiento (captura)
    file: captura4.png
  - name: Gimp en funcionamiento (captura)
    file: captura5.png
  - name: Gimp en funcionamiento (captura)
    file: captura6.png    

date: 04/04/19
